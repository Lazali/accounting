package a.Controller;

import a.Repository.AccountingRepository;
import a.model.Accounting;
import a.service.AccountingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class AccountingController {
    @Autowired
    private AccountingRepository accountingRepository;
    @Autowired
    AccountingService accountingService;

    //    @RequestMapping("/")
//    public String SpringBootSolrExample() {
//        return "Welcome ";
//    }
    @RequestMapping("/delete")
    public String deleteAllDocuments() {
        try { //delete all documents from solr core
            accountingRepository.deleteAll();
            return "documents deleted succesfully!";
        } catch (Exception e) {
            return "Failed to delete documents";
        }
    }

//    @RequestMapping("/save")
//    public String saveAllDocuments() {
////        documentRepository.save(document);
//        accountingRepository.save(Arrays.asList(new Accounting("1", "pdf", "Java Dev Zone"),
//                new Accounting("2", "msg", "subject:reinvetion"),
//                new Accounting("3", "pdf", "Spring boot sessions"),
//                new Accounting("4", "docx", "meeting agenda"),
//                new Accounting("5", "docx", "Spring boot + solr")));
//        return "5 documents saved!!!";
//    }

    //    @RequestMapping(value = "/accounting", method = RequestMethod.GET, produces = "application/json")
    @GetMapping(produces = "application/json")
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping({"/getAccountings"})
    public List<Accounting> getAllDocs() {
        List<Accounting> accountings = new ArrayList<>();
        // iterate all accountings and add it to list
        for (Accounting doc : this.accountingRepository.findAll()) {
            accountings.add(doc);
        }
        return accountings;
    }

    //    @DeleteMapping(path = { "/{id}" })
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping({"/deleteAccounting"})
    public void delete(@RequestBody Accounting accounting) {
        String id = accounting.getId();
        accountingService.delete(id);
//        accountingRepository.delete(String.valueOf(id));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping({"/createAccounting"})
    public Accounting create(@RequestBody Accounting accounting) throws IOException {
        accountingService.save(accounting);
        return accounting;
    }
//    @PostMapping(value = "/api/files")
//    @ResponseStatus(HttpStatus.OK)
//    public String handleFileUpload(@RequestParam("file") MultipartFile file) throws IOException {
//        return accountingService.storeFile(file);
//    }

}