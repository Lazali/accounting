package a.service;

import a.Repository.AccountingRepository;
import a.model.Accounting;
import org.apache.commons.io.Charsets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.URL;
import java.nio.Buffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Stream;

@Service
public class AccountingService {
    private static final String FILE_DIRECTORY = "/var/files";

    @Autowired
    private AccountingRepository accountingRepository;

    public void save(Accounting accounting) throws IOException {
//        FileInputStream fileInputStream = new FileInputStream(accounting.getFile());
//        int i;
//        StringBuffer stringBuffer = new StringBuffer();
//        while((i=fileInputStream.read())!= -1){
//                stringBuffer.append((char)i);
//        }
//        accounting.setFile(stringBuffer.toString());
        accountingRepository.save(accounting);
    }

    public void delete(String id) {
//        accountingRepository.findOne(id);
        accountingRepository.delete(id);
    }

//    public String storeFile(MultipartFile file) throws IOException {
//       /*
//       надо извлечь данные файла и записать их в переменную file
//        */
////        File file1 = new File(file);
////        FileInputStream fis = new FileInputStream(file);
////        byte[] data = new byte[(int) file.length()];
////        fis.read(data);
////        fis.close();
//
////        String str = new String(data, "UTF-8");
////        BufferedReader bufferedReader = new BufferedReader((FileReader) file);
////        String textFile = bufferedReader.readLine();
////        return textFile;
//
////        try (Stream<String> stream = Files.lines(Paths.get(String.valueOf(file)))) {
////            {
////                Optional<String> optional = stream.findFirst();
////                String value = optional.isPresent() ? optional.get() : null;
////            }
////        }
//
////        Path filePath = Paths.get(FILE_DIRECTORY + "/" + file.getOriginalFilename());
//
////        Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
//    }
    /*
    http://localhost:8983/solr/update?stream.body=<delete><query>id:12345</query></delete>&commit=true

     */
}
