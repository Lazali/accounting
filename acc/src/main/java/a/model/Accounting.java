package a.model;

import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.SolrDocument;

import java.io.File;

@SolrDocument(solrCoreName = "accounting")
public class Accounting {
    @Id
    @Field
    private String id;
    @Field
    private String file;
    @Field
    private String docType;

    @Field
    private String docTitle;

    public Accounting() {
    }

    public Accounting(String id, String docType, String docTitle, String file) {
        this.id = id;
        this.docTitle = docTitle;
        this.docType = docType;
        this.file = file;

//        fileScan = new File("C:/Users/samir.shahmuradli/Desktop/test.txt");
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Accounting{" +
                "id='" + id + '\'' +
                ", docType='" + docType + '\'' +
                ", docTitle='" + docTitle + '\'' +
                '}';
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getDocTitle() {
        return docTitle;
    }

    public void setDocTitle(String docTitle) {
        this.docTitle = docTitle;
    }


    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}