package a.Repository;

import a.model.Accounting;
import org.springframework.data.solr.repository.SolrCrudRepository;

public interface AccountingRepository extends SolrCrudRepository<Accounting, String> {
//    List<Accounting> findByDocTitleEndsWith(String title); // find documents whose docTitle ends with specified string
//    List<Accounting> findByDocTitleStartsWith(String title); // find documents whose docTitle starts with specified string
//    List<Accounting> findByDocTypeEndsWith(String type); //find documents whose docType ends with specified string
//    List<Accounting> findByDocTypeStartsWith(String type);//find documents whose docType start with specified string
}