import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Accounting, HttpClientService} from "../service/http-client.service";
import {FileUploader} from 'ng2-file-upload';
// import {copyFile} from "fs";

@Component({
  selector: 'app-add-accounting',
  templateUrl: './add-accounting.component.html',
  styleUrls: ['./add-accounting.component.css']
})
export class AddAccountingComponent implements OnInit {
  user: Accounting = new Accounting("", "", "", "");
  // @ViewChild('fileInput') fileInput: ElementRef;
  // // uploader: FileUploader;
  // isDropOver: boolean;

  constructor(private httpClientService: HttpClientService) {
  }

  ngOnInit() {
    // const headers = [{name: 'Accept', value: 'application/json'}];
    // this.uploader = new FileUploader({url: 'api/files', autoUpload: true, headers: headers});

    // this.uploader.onCompleteAll = () => alert('File uploaded');
  }

  createAccounting(): void {
    console.log(this.user);

    // copyFile(this.user.file, "../../assets/temp.txt");

    this.httpClientService.createAccounting(this.user)
      .subscribe(data => {
        alert("Accounting created successfully.");
      });

  };


}
