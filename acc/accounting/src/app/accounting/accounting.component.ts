import { Component, OnInit } from '@angular/core';
import {Accounting, HttpClientService} from "../service/http-client.service";

@Component({
  selector: 'app-accounting',
  templateUrl: './accounting.component.html',
  styleUrls: ['./accounting.component.css']
})
export class AccountingComponent implements OnInit {
  accountings:Accounting[];

  constructor(private httpClientService:HttpClientService) {
  }


  handleSuccessfulResponse(response)
  {
    this.accountings = response;
  }
  ngOnInit() {
    this.httpClientService.getAccounting().subscribe(response=>this.handleSuccessfulResponse(response),);
  }
  deleteAccounting(accounting:Accounting):void{
    this.httpClientService.deleteAccounting(accounting).subscribe(data=>{
      this.accountings = this.accountings.filter(u => u !== accounting);
      // alert("Delete Account");
    });
  };

}
