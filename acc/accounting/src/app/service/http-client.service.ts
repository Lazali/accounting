import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";


export class Accounting {
  constructor(
    public id: string,
    public docType: string,
    public docTitle: string,
    public file: string,
  ){}
}
@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  constructor(
    private httpClient:HttpClient
  ) { }
  getAccounting(){
    console.log("test call");
    return this.httpClient.get<Accounting[]>('http://localhost:8080/getAccountings')
  }
  public deleteAccounting(accounting) {
    return this.httpClient.delete<Accounting>("http://localhost:8080/deleteAccounting/", accounting);
  }

  public createAccounting(accounting) {
    return this.httpClient.post<Accounting>("http://localhost:8080/createAccounting/", accounting);
  }

}
