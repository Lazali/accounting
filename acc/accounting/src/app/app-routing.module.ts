import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AccountingComponent} from "./accounting/accounting.component";
import { AddAccountingComponent } from './add-accounting/add-accounting.component';


const routes: Routes = [{path:'', component:AccountingComponent},
  { path:'addaccounting', component: AddAccountingComponent},];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
